import { browser, by, element } from 'protractor';

export class PlayersPage {

  navigateTo() {
    return browser.get('/players') as Promise<any>;
  }
  getTitleText() {
    return element(by.id('pageTitleId')).getText() as Promise<string>;
  }
}
