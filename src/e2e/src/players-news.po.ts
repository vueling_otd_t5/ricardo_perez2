import { browser, by, element } from 'protractor';

export class PlayersPage {

  navigateTo(id: string) {
    return browser.get('/players/detail/' + id) as Promise<any>;
  }
  getTitleText() {
    return element(by.id('pageTitleId')).getText() as Promise<string>;
  }
}
