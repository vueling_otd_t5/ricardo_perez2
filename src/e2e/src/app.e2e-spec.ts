import { AppPage } from './app.po';
import { browser, logging, by, element } from 'protractor';
import { PlayersPage } from './players.po';

describe('E2E testing VuelingTest SPA', () => {
  let homePage: AppPage;
  let playersPage: PlayersPage;

  beforeEach(() => {
    homePage = new AppPage();
    homePage.navigateTo();
  });

  it('should display welcome message', () => {
    expect(homePage.getTitleText()).toEqual('Welcome to VuelingTest');
  });

  it('should display vy imagen at home page', async () => {
    const chkButton = homePage.findCheckBox();
    await chkButton.click();
    expect(homePage.vyImageIsPresent()).toBe(true);
    expect(homePage.nsImageIsPresent()).toBe(false);
  });

  it('should display ns imagen at home page', async () => {
    expect(homePage.nsImageIsPresent()).toBe(true);
    expect(homePage.vyImageIsPresent()).toBe(false);
  });

  it('should redirect to Player list', async () => {
    const listButton = homePage.findElement('playerListPage');

    await listButton.click();

    playersPage = new PlayersPage();
    expect(playersPage.getTitleText()).toEqual('List of players');
  });


  it('should select a team of the player list', async () => {
    const listButton = homePage.findElement('playerListPage');

    await listButton.click();

    playersPage = new PlayersPage();


    const elm = element(by.id('teamOptions'));
    browser.sleep(3000);
    await elm.element(by.css("option[value='WAS']")).click();
    const tableElement = element(by.css('.table'));
    expect(tableElement.isPresent()).toBe(true);
  });

  it('should select player and redirect to news page', async () => {
    const listButton = homePage.findElement('playerListPage');
    await listButton.click();

    playersPage = new PlayersPage();

    const elm = element(by.id('teamOptions'));
    browser.sleep(3000);
    await elm.element(by.css("option[value='WAS']")).click();

    await element(by.xpath('//table/tbody/tr[1]/td[4]/a')).click();

    expect(browser.getCurrentUrl()).toContain('detail');

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
