import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  findElement(elementId) {
    return element(by.css('#'+ elementId));
  }

  findCheckBox() {
    return element(by.css('input[type=checkbox]'));
  }

  getTitleText() {
    return element(by.id('welcomeId')).getText() as Promise<string>;
  }

  vyImageIsPresent() {
    return element(by.id('vyImg')).isPresent() as Promise<boolean>;
  }

  nsImageIsPresent() {
    return element(by.id('nsImg')).isPresent() as Promise<boolean>;
  }
}
