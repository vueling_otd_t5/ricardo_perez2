export const environment = {
  production: true,
  apiKey: '176da0bfb94348a2917e110712f1b698',
  teamsEndpointUrl: 'https://fly.sportsdata.io/v3/nba/scores/json/teams',
  playersEndpointUrl: 'https://fly.sportsdata.io/v3/nba/stats/json/Players',
  playerNewsEndpointUrl: 'https://fly.sportsdata.io/v3/nba/scores/json/NewsByPlayerID'
};

