export const environment = {
  production: false,
  apiKey: '176da0bfb94348a2917e110712f1b698',
  teamsEndpointUrl: 'https://fly.sportsdata.io/v3/nba/scores/json/teams',
  playersEndpointUrl: 'https://fly.sportsdata.io/v3/nba/stats/json/Players',
  playerNewsEndpointUrl: 'https://fly.sportsdata.io/v3/nba/scores/json/NewsByPlayerID'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
