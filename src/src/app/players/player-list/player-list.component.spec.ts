import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { BehaviorSubject, of } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';
import { PlayerTableComponent } from './components/player-table/player-table.component';
import { TeamsComponent } from './components/teams/teams.component';
import { IPlayer } from './models/IPlayer';

import { PlayerListComponent } from './player-list.component';
import { PlayerService } from './services/players.service';

describe('PlayerListComponent', () => {
  let component: PlayerListComponent;
  let fixture: ComponentFixture<PlayerListComponent>;
  const playerSerivceMock = jasmine.createSpyObj(['getPlayersByTeam']);

  const mockedValue = 'WAS';
  const storeSubjectMock = new BehaviorSubject(mockedValue);
  const mockedStore = {
    pipe: () => storeSubjectMock.asObservable(),
  };

  playerSerivceMock.getPlayersByTeam.and.returnValue(of([{
    id: '1',
    birthDate: undefined,
    firstName: 'F',
    lastName: 'S'
  }] as IPlayer[]));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerListComponent, TeamsComponent, PlayerTableComponent ],
      imports: [SharedModule,  RouterTestingModule.withRoutes([
        { path: 'players', component: PlayerListComponent }
       ])],
       providers: [
        {
          provide: Store,
          useValue: mockedStore
        },
        {
          provide: PlayerService,
          useValue: playerSerivceMock
        },
       ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get the selected team from the store', async () => {
    await fixture.whenStable();

    expect(component.selectedTeam).toBe('WAS');
  });
});
