import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { IPlayer } from './models/IPlayer';
import { PlayerService } from './services/players.service';
import * as playersState from './state/players.reducer';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit, OnDestroy  {
  pageTitle = 'List of players';
  selectedTeam = '';
  playersList: IPlayer[] = [];
  activeComponent = true;

  sub!: Subscription;
  constructor(private playerService: PlayerService,
              private store: Store<playersState.State>) { }

  ngOnInit() {
    this.store
      .pipe(select(playersState.getPlayersTeamSelected),
              takeWhile(() => this.activeComponent))
      .subscribe((selectedTeam) => {
      if (selectedTeam) {
        this.selectedTeam = selectedTeam;
        this.retrievePlayers(this.selectedTeam);
      }
    });
  }


  public retrievePlayers(team: string) {
    this.sub = this.playerService.getPlayersByTeam(team).subscribe({
      next: players => {
        this.playersList = players;
      },
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    this.activeComponent = false;
  }
}
