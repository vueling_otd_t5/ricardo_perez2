import { Resource } from 'src/app/shared/services/resource';

export interface ITeam extends Resource {
  key: string;
  city: string;
  name: string;
  fullName: string;
}
