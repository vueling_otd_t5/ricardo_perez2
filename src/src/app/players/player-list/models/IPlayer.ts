import { Resource } from 'src/app/shared/services/resource';

export interface IPlayer extends Resource {
firstName: string;
lastName: string;
fullName: string;
birthDate: Date;
}
