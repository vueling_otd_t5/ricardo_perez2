import { IPlayer } from '../models/IPlayer';

export class PlayerSerializer {
  fromJson(json: any): IPlayer {
    return {
      id: json.PlayerID,
      firstName: json.FirstName,
      lastName: json.LastName,
      fullName: json.FirstName + ' ' + json.LastName,
      birthDate: json.BirthDate,
    };
  }
}
