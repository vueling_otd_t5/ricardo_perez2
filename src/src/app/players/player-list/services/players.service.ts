import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/shared/services/api.service';
import { environment } from 'src/environments/environment';
import { IPlayer } from '../models/IPlayer';
import { PlayerSerializer } from './players.serializer';

@Injectable({
  providedIn: 'root'
})
export class PlayerService extends ApiService<IPlayer> {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getPlayersByTeam(team: string): Observable<IPlayer[]> {
    return this.list(environment.playersEndpointUrl, new PlayerSerializer(), team);
  }
}
