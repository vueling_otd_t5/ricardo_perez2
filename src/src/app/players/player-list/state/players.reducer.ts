import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../../state/app-store';
import { ITeam } from '../models/ITeam';
import { PlayerActions, PlayersActionTypes } from './actions/players.actions';

export interface State  extends fromRoot.State {
  players: PlayerState;
}

export interface PlayerState {
  teams: ITeam[];
  selectedTeam: string;
}

const initialPlayersState: PlayerState = {
    teams: [],
    selectedTeam: ''
  };


const getPlayersFeatureState = createFeatureSelector<PlayerState>('players');

export const getPlayersTeams = createSelector( getPlayersFeatureState, state => state.teams);
export const getPlayersTeamSelected = createSelector(getPlayersFeatureState, state => state.selectedTeam);

export function reducer(state = initialPlayersState, action: PlayerActions): PlayerState {

  switch (action.type) {
    case PlayersActionTypes.SelectTeam:
      return {
        ...state,
          selectedTeam: action.payload
      };
    case PlayersActionTypes.LoadTeamsSuccess:
      return {
        ...state,
          teams: action.payload
      };
    default:
      return state;
  }
}
