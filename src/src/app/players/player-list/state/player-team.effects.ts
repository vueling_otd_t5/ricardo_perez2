import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { TeamsService } from '../components/teams/services/team.service';
import * as playerTeamActions from './actions/players.actions';

@Injectable()
export class PlayerTeamEffects {

  constructor(private actions$: Actions, private teamsService: TeamsService) {

  }

  @Effect()
  loadTeams$ = this.actions$.pipe(
    ofType(playerTeamActions.PlayersActionTypes.LoadTeams),
    mergeMap((action: playerTeamActions.LoadTeams) => this.teamsService.getTeams().pipe(
      map(p => (new playerTeamActions.LoadTeamsSuccess(p)))
      )
    )
  );

}
