import { Action } from '@ngrx/store';
import { ITeam } from '../../models/ITeam';

export enum PlayersActionTypes {
  LoadTeams = '[Players] Load Teams',
  LoadTeamsSuccess = '[Players] Load Teams success',
  LoadTeamsFail = '[Players] Load Teams fail',
  SelectTeam = '[Players] Select a team'
}


export class LoadTeams implements Action {
  readonly type = PlayersActionTypes.LoadTeams;

}


export class LoadTeamsSuccess implements Action {
  readonly type = PlayersActionTypes.LoadTeamsSuccess;

  constructor(public payload: ITeam[]) {
  }

}

export class LoadTeamsFail implements Action {
  readonly type = PlayersActionTypes.LoadTeamsFail;

  constructor(public payload: string) {

  }
}


export class SelectTeam implements Action {
  readonly type = PlayersActionTypes.SelectTeam;

  constructor(public payload: string) {
  }

}


export type PlayerActions = LoadTeams
| LoadTeamsSuccess
| LoadTeamsFail
| SelectTeam;
