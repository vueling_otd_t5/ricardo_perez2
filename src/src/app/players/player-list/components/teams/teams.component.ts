import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { State } from 'src/app/state/app-store';
import * as PlayerActions from '../../state/actions/players.actions';
import * as fromPlayer from '../../state/players.reducer';
import { ITeam } from '../../models/ITeam';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit, OnDestroy {

activeComponent = true;
  public teamsForm = new FormGroup({
    team: new FormControl(''),
});
isDataLoaded = false;
teams: ITeam[] = [];
  constructor(private store: Store<State>) { }

  ngOnDestroy(): void {
    this.activeComponent = false;
  }

  ngOnInit() {
    this.store
    .pipe(select(fromPlayer.getPlayersTeams),
    takeWhile(() => this.activeComponent))
    .subscribe((teamsFromStore) => {
      if (teamsFromStore && teamsFromStore.length > 0) {
        if (Array.isArray(teamsFromStore)) {
          this.teams = teamsFromStore;
          this.isDataLoaded = true;
        }
      } else {
        this.store.dispatch(new PlayerActions.LoadTeams());
      }
    });

    this.store
    .pipe(select(fromPlayer.getPlayersTeamSelected),
    takeWhile(() => this.activeComponent))
    .subscribe((selectedTeamFromStore) => {
      if (selectedTeamFromStore && (typeof selectedTeamFromStore === 'string')) {
        this.teamsForm.get('team').setValue(selectedTeamFromStore);
      }
    });
  }

  selectedTeamOption(name) {
    if (name.value) {
      this.store.dispatch(new PlayerActions.SelectTeam(name.value));
    }
  }
}
