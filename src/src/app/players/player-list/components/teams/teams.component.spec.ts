import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { BehaviorSubject } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';
import { ITeam } from '../../models/ITeam';

import { TeamsComponent } from './teams.component';

describe('TeamsComponent', () => {
  let component: TeamsComponent;
  let fixture: ComponentFixture<TeamsComponent>;

  const mockedValue: ITeam[] = [{
    id: '1',
    city: 'city',
    fullName: 'team1',
    key: 'key1',
    name: 'name'
  }];
  const storeSubjectMock = new BehaviorSubject(mockedValue);
  const mockedStore = {
    pipe: () => storeSubjectMock.asObservable(),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsComponent ],
      imports: [SharedModule],
      providers: [
        {
          provide: Store,
          useValue: mockedStore
        }
       ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get the list of teams from the store', async () => {
    await fixture.whenStable();
    expect(component.teams[0].fullName).toBe('team1');
  });

});

