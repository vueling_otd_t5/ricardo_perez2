import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/shared/services/api.service';
import { environment } from 'src/environments/environment';
import { ITeam } from '../../../models/ITeam';
import { TeamSerializer } from './team.serializer';

@Injectable({
  providedIn: 'root'
})
export class TeamsService extends ApiService<ITeam> {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getTeams(): Observable<ITeam[]> {
    return this.list(environment.teamsEndpointUrl, new TeamSerializer());
  }
}
