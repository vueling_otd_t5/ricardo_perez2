import { ITeam } from '../../../models/ITeam';

export class TeamSerializer {
  fromJson(json: any): ITeam {
    return {
      id: json.TeamID,
      key: json.Key,
      name: json.Name,
      city: json.City,
      fullName: json.City + ' ' + json.Name,
    };
  }
}
