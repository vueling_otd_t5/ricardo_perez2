import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { PlayerDetailComponent } from 'src/app/players/player-detail/player-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PlayerTableComponent } from './player-table.component';

describe('PlayerTableComponent', () => {
  let component: PlayerTableComponent;
  let fixture: ComponentFixture<PlayerTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerTableComponent ],
      imports: [SharedModule,  RouterTestingModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerTableComponent);
    component = fixture.componentInstance;
    component.playersList = [{
      id: '1',
      birthDate: new Date('1950-1-1'),
      firstName: 'Larry',
      lastName: 'Bird',
      fullName: 'Larry Bird'
    }];
    fixture.detectChanges();
  });

  it('should receive a list of player and diplay it in the HTML', () => {

    expect(fixture.debugElement.queryAll(By.css('tr')).length).toBe(2);
  });

});
