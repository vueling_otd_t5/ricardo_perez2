import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { IPlayer } from '../../models/IPlayer';

@Component({
  selector: 'app-player-table',
  templateUrl: './player-table.component.html',
  styleUrls: ['./player-table.component.css']
})
export class PlayerTableComponent implements OnInit {

  @Input()
  playersList: IPlayer[];
  constructor() { }

  ngOnInit() {
  }

}
