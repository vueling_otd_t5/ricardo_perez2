import { Resource } from 'src/app/shared/services/resource';

export interface IPlayerNews extends Resource {
title: string;
content: string;
originalSourceUrl: string;
}

