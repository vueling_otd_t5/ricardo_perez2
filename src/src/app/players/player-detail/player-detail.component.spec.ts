import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';
import { IPlayerNews } from './models/IPlayerNews';

import { PlayerDetailComponent } from './player-detail.component';
import { PlayerNewsService } from './services/player-news.service';

describe('PlayerDetailComponent', () => {
  let component: PlayerDetailComponent;
  let fixture: ComponentFixture<PlayerDetailComponent>;
  const playerNewsSerivceMock: jasmine.SpyObj<PlayerNewsService> = jasmine.createSpyObj(['getPlayerNews']);

  playerNewsSerivceMock.getPlayerNews.and.returnValue(of({
    id: '1',
    content: 'content',
    originalSourceUrl: 'url',
    title: 'title'
  } as IPlayerNews));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerDetailComponent ],
      imports: [SharedModule, RouterTestingModule],
      providers: [{
        provide: PlayerNewsService,
        useValue: playerNewsSerivceMock
      },
      {
        provide: ActivatedRoute,
        useValue: {
          params: of({id: '123'})
        }
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should evalute the param in Url', async () => {
    expect(component.playerId).toBe('123');
  });

  it('should the PlayerNewsService return a valid title news', async () => {
    expect(component.playerNews.title).toBe('title');
  });
});
