import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IPlayerNews } from './models/IPlayerNews';
import { PlayerNewsService } from './services/player-news.service';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit, OnDestroy {
  sub!: Subscription;
  playerId: string;
  playerNews: IPlayerNews;
  isDataLoaded = false;
  areNewsAvailable = true;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private playerNewsService: PlayerNewsService) { }


  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.playerId = params['id'];
      this.retrievePlayerNews();
    });
  }

  public retrievePlayerNews() {
    this.sub = this.playerNewsService.getPlayerNews(this.playerId)
    .subscribe(
      result => {
        this.areNewsAvailable = true;
        this.playerNews = result;
        this.isDataLoaded = true;
      },
      error => {
        this.areNewsAvailable = false;
        this.isDataLoaded = true;
    },
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


  onBack(): void {
    this.router.navigate(['./players']);
  }
}
