import { IPlayerNews } from '../models/IPlayerNews';

export class PlayerNewsSerializer {
  fromJson(json: any): IPlayerNews {
    return {
      id: json[0].NewsID,
      content: json[0].Content,
      originalSourceUrl: json[0].OriginalSourceUrl,
      title: json[0].Title
    };
  }
}
