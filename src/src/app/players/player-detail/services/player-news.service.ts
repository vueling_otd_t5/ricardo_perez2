import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/shared/services/api.service';
import { environment } from 'src/environments/environment';
import { IPlayerNews } from '../models/IPlayerNews';
import { PlayerNewsSerializer } from './player-news.serializer';

@Injectable({
  providedIn: 'root'
})
export class PlayerNewsService extends ApiService<IPlayerNews> {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getPlayerNews(id: string): Observable<IPlayerNews> {
    return this.get(environment.playerNewsEndpointUrl, new PlayerNewsSerializer(), id);
  }
}
