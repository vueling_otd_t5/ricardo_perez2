import { fakeAsync, inject, TestBed } from '@angular/core/testing';
import { PlayerNewsService } from './player-news.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

describe('PlayerNews service', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PlayerNewsService]
      });
    });

    it('should retrieve data from player news service Ok', inject(
      [PlayerNewsService, HttpTestingController],
      (service: PlayerNewsService, controller: HttpTestingController)  => {
        const expectedPlayerNews = [{NewsID: 67721, Title: 'title', Content: 'content', OriginalSourceUrl: 'url'}];

        service.getPlayerNews('123').subscribe(news => {
          expect(news.content).toBe(expectedPlayerNews[0].Content);
        });
        // assert
        const req = controller
        .expectOne( environment.playerNewsEndpointUrl + '/123?key=' + environment.apiKey);
        req.flush(expectedPlayerNews);
      })
    );

    it('should retrieve data from player news service Ko', inject(
      [PlayerNewsService, HttpTestingController],
      (service: PlayerNewsService, controller: HttpTestingController)  => {
        const emsg = 'deliberate 500 error';

        service.getPlayerNews('123').subscribe(
          data => fail('should have failed with the 500 error'),
          (error: HttpErrorResponse) => {
            expect(error).toContain('500');
          }
          );
        // assert
        const req = controller
        .expectOne( environment.playerNewsEndpointUrl + '/123?key=' + environment.apiKey);
        // req.flush(expectedPlayerNews);
        req.flush(emsg, { status: 500, statusText: 'Internal error' });
      })
    );

    });
