import { NgModule } from '@angular/core';
import { PlayerListComponent } from './player-list/player-list.component';
import { PlayerDetailComponent } from './player-detail/player-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { TeamsComponent } from './player-list/components/teams/teams.component';
import { EffectsModule } from '@ngrx/effects';
import { PlayerTeamEffects } from './player-list/state/player-team.effects';
import { StoreModule } from '@ngrx/store';
import { reducer } from './player-list/state/players.reducer';
import { PlayerTableComponent } from './player-list/components/player-table/player-table.component';

const routes: Routes = [
  {
   path: '',
   children: [
    {path : 'detail/:id', component : PlayerDetailComponent},
    {path : 'list', component : PlayerListComponent},
    {path : '**', component : PlayerListComponent},
   ]
  }

  ];

@NgModule({
  declarations: [PlayerListComponent, PlayerDetailComponent, TeamsComponent, PlayerTableComponent],
  entryComponents: [PlayerListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('players', reducer),
    EffectsModule.forFeature([PlayerTeamEffects])

  ]
})
export class PlayersModule { }

