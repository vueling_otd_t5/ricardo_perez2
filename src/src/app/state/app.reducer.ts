import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './app-store';
import { AppActions, AppActionTypes } from './app.actions';


const initialState: State = {
showVuelingImage: false
};


const getAppFeatureState = createFeatureSelector<State>('app');

export const getShowVuelingImage = createSelector(
    getAppFeatureState,
    state => state.showVuelingImage);

export function reducer(state = initialState, action: AppActions): State {

  switch (action.type) {
    case AppActionTypes.ToggleImage:
      return {
        ...state,
          showVuelingImage: action.payload
      };

    default:
      return state;
  }
}
