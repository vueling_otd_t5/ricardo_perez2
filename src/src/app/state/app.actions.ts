import { Action } from '@ngrx/store';

export enum AppActionTypes {
  ToggleImage = '[App] toggle home page image'
}

export class ToggleImageAppCode implements Action {
  readonly type = AppActionTypes.ToggleImage;

  constructor(public payload: boolean) {}

}

export type AppActions = ToggleImageAppCode;
