import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Resource } from './resource';
import { Serializer } from './serializer';

@Injectable()
export abstract class ApiService<T extends Resource> {
    constructor(
      private httpClient: HttpClient
      ) {}

   /**
    * Retrieve a list a element of an endpoint and mapped to a model defined in the concrecte class
    * @param endpoint the url
    * @param serializer the serializer mapper class
    * @param resource the parameter to retrieve the resource e.g the PlayerID
    * @returns Return an observable mapped to the concrete class defined in the service
    */
   protected get(endpoint: string, serializer: Serializer, resource: string): Observable<T> {
    return this.httpClient
    .get(`${endpoint}/${resource}?key=${environment.apiKey}`)
    .pipe(map((data: any) => this.convertData(serializer, data)),
    catchError(this.handleError));
  }

  /**
   * Retrieve a list of elements of an endpoint and mapped to a model defined in the concrecte class
   * @param endpoint the url
   * @param serializer the serializer mapper class
   * @param resource (optional) if the endpoint need a resource like team name e.g WAS
   * @returns Return an observable mapped to the concrete class defined in the service
   */
  protected list(endpoint: string, serializer: Serializer, resource?: string): Observable<T[]> {
  const  resourceToSearch = resource  ? `/${resource}` : '';
  return this.httpClient
      .get(`${endpoint}${resourceToSearch}?key=${environment.apiKey}`)
      .pipe(map((data: any) => this.convertDataToArray(serializer, data) ),
      catchError(this.handleError));
    }

  private convertDataToArray(serializer: Serializer, data: any): T[] {
    return data.map(item => serializer.fromJson(item) );
  }

  private convertData(serializer: Serializer, data: any): T {
    return serializer.fromJson(data) as T;
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    if(!environment.production) {
      console.log(errorMessage);
    }
    return throwError(errorMessage);
  }
}
