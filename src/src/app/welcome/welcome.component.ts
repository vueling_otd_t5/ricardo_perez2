import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { State } from '../state/app-store';
import { ToggleImageAppCode } from '../state/app.actions';
import { getShowVuelingImage } from '../state/app.reducer';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  showVuelingImage: boolean;

  constructor(private store: Store<State>) {

   }

   ngOnInit() {
     this.store.select(getShowVuelingImage).subscribe((showVuelingImage) => {
         this.showVuelingImage = showVuelingImage;
     });
  }
  checkChanged(value: boolean) {
    this.store.dispatch(new ToggleImageAppCode(value));
  }
}


