import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { ToggleImageAppCode } from '../state/app.actions';

import { WelcomeComponent } from './welcome.component';

describe('WelcomeComponent', () => {
  let component: WelcomeComponent;
  let fixture: ComponentFixture<WelcomeComponent>;
  const storeMock = jasmine.createSpyObj(['dispatch', 'select']);
  storeMock.select.and.returnValue(of(false));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeComponent ],
      providers: [
        {
          provide: Store,
          useValue: storeMock
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set showVuelingImage property as false', async () => {

    await fixture.whenStable();

    expect(component.showVuelingImage).toBe(false);
  });

  it('should call checkChanged with parameters Ok', async () => {

    await fixture.whenStable();
    component.checkChanged(true);

    expect(storeMock.dispatch).toHaveBeenCalledWith(new ToggleImageAppCode(true));
  });


});
