import { Component } from '@angular/core';
import { OnDemandPreloadService } from './strategies/on-demand-preload.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-vueling';

  constructor(private preloadOnDemandService: OnDemandPreloadService) {

  }

  /**
   * Trigger the bundle loading strategy to preload a bundle (if it´s not loaded yet)
   * @param routePath path that want to be loaded
   */
  preloadBundle(routePath) {
    this.preloadOnDemandService.startPreload(routePath);
  }
}
