import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnDemandPreloadStrategy } from './strategies/on-demand.strategy';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {path : 'players', loadChildren: './players/players.module#PlayersModule' },
  {path : '', component : WelcomeComponent},
  {path : '**', component : WelcomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: OnDemandPreloadStrategy
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
