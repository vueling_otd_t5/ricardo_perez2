Develop an SPA in Angular 4 that shows NBA statistics using this api: https://stats.nba.com/stats

The aplication should show a list of players indicating their name, surname, equipment and their age.

For this case, the following endpoint will be user: **leaguedashplayerbiostats**

Parameters:

- PerMode: "Totals"
- LeagueID: NBA = 00, ABA = 01
- Season Format NNNN-NN (ex. 2016-17)
- SeasonType: One of "Regular Season", "Pre Season", "Playoffs", "All-Star", "All Star", "Preseason"

When you select a player, display a screen with the player's data (Name, Surname and date of birth in format: Sep 3, 2010) using the following endpoint: **commonplayerinfo**

Parameters:

- PlayerID

Do not focus your efforts in the UI or UX. The important part is the readability of the code, as well as the architecture and maintainability. The development of tests will be evaluated.

**Please, write "Finished" on final commit comment, this comment inform us that is finished and we can proceed to validate the code.**

# Solution

The application has two main pages: a list of player filtered by its team and a detail page that retrieve news about a selected player.

![App mockups](https://bitbucket.org/vueling_otd_t5/fernando_snchez/raw/c67806cc5e2f49da26d59cedd5976ae7406d9a79/imgs/vuelingApp.png)


## Endpoints

The endpoint of the teams. It´s use to populate the Active teams dropdownlist

https://sportsdata.io/developers/api-documentation/nba#/endpoint/teams-active

Given a team, it returns the list of its players (Example with WAS team -Washington wizards-)

https://sportsdata.io/developers/api-documentation/nba#/endpoint/players-by-team

Given a player, it brings you the latest news that exists on social networks

https://sportsdata.io/developers/api-documentation/nba#/endpoint/news-by-player


## Preloading strategy
 
The application is made with Angular@7.
The application makes use of lazy loading and uses to load the bundle of the Player module a custom strategy called `OnDemandPreloadStrategy` that is applied when the user perform a mouse over the Player menu.

## Redux pattern

To maintain the state of the application, the redux pattern is used with the ngrx@7 library.
3 specific states are saved:

- Home page checkbox
- Team selected
- List of teams


```json
{
  app: {
    showVuelingImage: false
  },
  players: {
    teams: [
      {
        id: 1,
        key: 'WAS',
        name: 'Wizards',
        city: 'Washington',
        fullName: 'Washington Wizards'
      },
      {
        id: 2,
        key: 'CHA',
        name: 'Hornets',
        city: 'Charlotte',
        fullName: 'Charlotte Hornets'
      }
    ],
    selectedTeam: 'ATL'
  }
}
```


## Testing E2E

Tha application contain 6 e2e testing made with Protractor library.

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

![E2E](https://bitbucket.org/vueling_otd_t5/fernando_snchez/raw/d5a55a33fc1ea84b234699c5582a8cd692370c82/imgs/e2e_testing.png)


## Unit Testing

Tha application contain 11 unit testing made with Jasmine library.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

![Unit testing](https://bitbucket.org/vueling_otd_t5/fernando_snchez/raw/d5a55a33fc1ea84b234699c5582a8cd692370c82/imgs/unit_test.png)

